﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace BlazorStatTracker.Dashboard
{
  public class VideoManager
  {
    public Video? SelectedVideo { get; set; }
    private readonly HttpClient _httpClient;

    public VideoManager(HttpClient httpClient)
    {
      _httpClient = httpClient;
    }

    public async Task<ICollection<Video>> GetVideos()
    {
      var videos = await _httpClient.GetFromJsonAsync<Video[]>("/videos");
      if (Array.Find(videos, (video => video.Id == SelectedVideo?.Id)) is null && videos.Length > 0)
      {
        SelectedVideo = videos[0];
      }
      return videos;
    }

    public async Task<ICollection<Video>> GetVideos(string filterTerm)
    {
      var fullList = await _httpClient.GetFromJsonAsync<Video[]>("/videos");
      var filteredVideos = Array.FindAll(fullList, video => video.Title.Contains(filterTerm));

      if (Array.Find(filteredVideos, (video => video.Id == SelectedVideo?.Id)) is null && filteredVideos.Length > 0)
      {
        SelectedVideo = filteredVideos[0];
      }
      return filteredVideos;
    }

  }

  public class Video
  {
    public string Id { get; set; }
    public string Title { get; set; }
    public string Author { get; set; }

    public Video() { }

    public Video(string _id, string _title, string _author)
    {
      Id = _id;
      Title = _title;
      Author = _author;
    }

  }
}
