using BlazorStatTracker.Dashboard;

using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlazorStatTracker
{
  public class Program
  {
    public static async Task Main(string[] args)
    {
      WebAssemblyHostBuilder builder = WebAssemblyHostBuilder.CreateDefault(args);
      builder.RootComponents.Add<App>("#app");

      builder.Services.AddSingleton(sp => new HttpClient { BaseAddress = new Uri("https://api.angularbootcamp.com") });
      builder.Services.AddSingleton<VideoManager>();

      await builder.Build().RunAsync();
    }
  }
}
